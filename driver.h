#ifndef _DRIVER_
#define _DRIVER_

#include <ntddk.h>

#define DRIVER_NAME L"DKF"


NTSTATUS DriverEntry( IN PDRIVER_OBJECT  DriverObject, IN PUNICODE_STRING RegistryPath );
VOID DriverUnload(IN PDRIVER_OBJECT DriverObject);
NTSTATUS DriverDispatch(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp);
NTSTATUS DrvWrite(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp);
NTSTATUS DrvCreateClose(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp);
NTSTATUS Hook_Function();
void Unhook_fonction();
NTSTATUS Un_Hook_Function();


#pragma pack(1)
typedef struct ServiceDescriptorEntry {
        unsigned int *ServiceTableBase;
        unsigned int *ServiceCounterTableBase; //Used only in checked build
        unsigned int NumberOfServices;
        unsigned char *ParamTableBase;
} ServiceDescriptorTableEntry_t, *PServiceDescriptorTableEntry_t;
#pragma pack()

__declspec(dllimport)  ServiceDescriptorTableEntry_t KeServiceDescriptorTable;
#define SYSTEMSERVICE(_function)  KeServiceDescriptorTable.ServiceTableBase[ *(PULONG)((PUCHAR)_function+1)]

PMDL  g_pmdlSystemCall;
PVOID *MappedSystemCallTable;


// inspiré par http://www.sevagas.com/?Hide-files-using-SSDT-hooking

/* Prototype of original routine */
NTSYSAPI
NTSTATUS
NTAPI ZwQueryDirectoryFile
(
  IN    HANDLE FileHandle,
  IN    HANDLE Event,
  IN    PIO_APC_ROUTINE ApcRoutine,
  IN    PVOID ApcContext,
  OUT   PIO_STATUS_BLOCK IoStatusBlock,
  OUT   PVOID FileInformation,
  IN    ULONG Length,
  IN    FILE_INFORMATION_CLASS FileInformationClass,
  IN    BOOLEAN ReturnSingleEntry,
  IN    PUNICODE_STRING FileName,
  IN    BOOLEAN RestartScan
);

/* Function pointer declaration and definition */
typedef NTSTATUS  (*ZwQueryDirectoryFilePtr)
(
  IN    HANDLE FileHandle,
  IN    HANDLE Event,
  IN    PIO_APC_ROUTINE ApcRoutine,
  IN    PVOID ApcContext,
  OUT   PIO_STATUS_BLOCK IoStatusBlock,
  OUT   PVOID FileInformation,
  IN    ULONG Length,
  IN    FILE_INFORMATION_CLASS FileInformationClass,
  IN    BOOLEAN ReturnSingleEntry,
  IN    PUNICODE_STRING FileName,
  IN    BOOLEAN RestartScan
);
ZwQueryDirectoryFilePtr oldZwQueryDirectoryFile;

// the prototype of our replacement function
NTSTATUS newZwQueryDirectoryFile
(
  IN    HANDLE FileHandle,
  IN    HANDLE Event,
  IN    PIO_APC_ROUTINE ApcRoutine,
  IN    PVOID ApcContext,
  OUT   PIO_STATUS_BLOCK IoStatusBlock,
  OUT   PVOID FileInformation,
  IN    ULONG Length,
  IN    FILE_INFORMATION_CLASS FileInformationClass,
  IN    BOOLEAN ReturnSingleEntry,
  IN    PUNICODE_STRING FileName,
  IN    BOOLEAN RestartScan
);

 

// now we must define the format of various FILE_INFORMATION_CLASS structures
// that can be returned by ZwQueryDirectoryFile 
// mais heureusement, elles ont toutes en commun les champs FileName et NextEntryOffset
// * FileName : le nom du fichier: ".", "..", "toto.txt", ...
// * NextEntryOffset : pointeur vers la prochaine structure FileInformation de la liste chainée
typedef struct _FILE_BOTH_DIR_INFORMATION {
  ULONG         NextEntryOffset;
  ULONG         FileIndex;
  LARGE_INTEGER CreationTime;
  LARGE_INTEGER LastAccessTime;
  LARGE_INTEGER LastWriteTime;
  LARGE_INTEGER ChangeTime;
  LARGE_INTEGER EndOfFile;
  LARGE_INTEGER AllocationSize;
  ULONG         FileAttributes;
  ULONG         FileNameLength;
  ULONG         EaSize;
  CCHAR         ShortNameLength;
  WCHAR         ShortName[12];
  WCHAR         FileName[1];
} FILE_BOTH_DIR_INFORMATION, *PFILE_BOTH_DIR_INFORMATION;
 
typedef struct _FILE_ID_BOTH_DIR_INFORMATION {
  ULONG         NextEntryOffset;
  ULONG         FileIndex;
  LARGE_INTEGER CreationTime;
  LARGE_INTEGER LastAccessTime;
  LARGE_INTEGER LastWriteTime;
  LARGE_INTEGER ChangeTime;
  LARGE_INTEGER EndOfFile;
  LARGE_INTEGER AllocationSize;
  ULONG         FileAttributes;
  ULONG         FileNameLength;
  ULONG         EaSize;
  CCHAR         ShortNameLength;
  WCHAR         ShortName[12];
  LARGE_INTEGER FileId;
  WCHAR         FileName[1];
} FILE_ID_BOTH_DIR_INFORMATION, *PFILE_ID_BOTH_DIR_INFORMATION;
 
typedef struct _FILE_ID_FULL_DIR_INFORMATION {
  ULONG         NextEntryOffset;
  ULONG         FileIndex;
  LARGE_INTEGER CreationTime;
  LARGE_INTEGER LastAccessTime;
  LARGE_INTEGER LastWriteTime;
  LARGE_INTEGER ChangeTime;
  LARGE_INTEGER EndOfFile;
  LARGE_INTEGER AllocationSize;
  ULONG         FileAttributes;
  ULONG         FileNameLength;
  ULONG         EaSize;
  LARGE_INTEGER FileId;
  WCHAR         FileName[1];
} FILE_ID_FULL_DIR_INFORMATION, *PFILE_ID_FULL_DIR_INFORMATION;
 
typedef struct _FILE_DIRECTORY_INFORMATION {
  ULONG         NextEntryOffset;
  ULONG         FileIndex;
  LARGE_INTEGER CreationTime;
  LARGE_INTEGER LastAccessTime;
  LARGE_INTEGER LastWriteTime;
  LARGE_INTEGER ChangeTime;
  LARGE_INTEGER EndOfFile;
  LARGE_INTEGER AllocationSize;
  ULONG         FileAttributes;
  ULONG         FileNameLength;
  WCHAR         FileName[1];
} FILE_DIRECTORY_INFORMATION, *PFILE_DIRECTORY_INFORMATION;
 
typedef struct _FILE_FULL_DIR_INFORMATION {
  ULONG         NextEntryOffset;
  ULONG         FileIndex;
  LARGE_INTEGER CreationTime;
  LARGE_INTEGER LastAccessTime;
  LARGE_INTEGER LastWriteTime;
  LARGE_INTEGER ChangeTime;
  LARGE_INTEGER EndOfFile;
  LARGE_INTEGER AllocationSize;
  ULONG         FileAttributes;
  ULONG         FileNameLength;
  ULONG         EaSize;
  WCHAR         FileName[1];
} FILE_FULL_DIR_INFORMATION, *PFILE_FULL_DIR_INFORMATION;
 
typedef struct _FILE_NAMES_INFORMATION {
  ULONG NextEntryOffset;
  ULONG FileIndex;
  ULONG FileNameLength;
  WCHAR FileName[1];
} FILE_NAMES_INFORMATION, *PFILE_NAMES_INFORMATION;



#endif